package co.com.worldoffice.util;

public class Util 
{
	public static boolean validarCampoNulo(String campo)
	{
		return campo == null || campo.trim().length() == 0;
	}

}
