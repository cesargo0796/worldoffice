package co.com.worldoffice.controller;

import java.util.List;
import java.util.Random;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.worldoffice.model.CarritoCompra;
import co.com.worldoffice.model.Producto;
import co.com.worldoffice.repository.CarritoCompraRepository;
import co.com.worldoffice.repository.ProductoRepository;
import co.com.worldoffice.util.Util;

@RestController
@RequestMapping("/WorldOffice/CarritoCompra/V1.0")
public class CarritoCompraController {
    
    @Autowired
    private ProductoRepository productoRepository;
    
    @Autowired
    private CarritoCompraRepository carritoCompraRepository;
    
    private static final Logger LOGGER = LogManager.getRootLogger();

	@GetMapping(path = "/GetProducts", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<?> getProducts(
    		@RequestParam (value = "nombre", required = false) String nombre,
    		@RequestParam (value = "minPrecio", required = false) Double minPrecio,
    		@RequestParam (value = "maxPrecio", required = false) Double maxPrecio,
    		@RequestParam (value = "marca", required = false) String marca
    		) 
    		throws Exception 
	{
		LOGGER.info("Inicio de consulta de productos");
		if(Util.validarCampoNulo(nombre) && 
		   Util.validarCampoNulo(marca) &&
		   minPrecio == null &&
		   maxPrecio == null)
		{
			return productoRepository.findAll();			
		}
		if(!Util.validarCampoNulo(nombre) && 
		   !Util.validarCampoNulo(marca) &&
		   minPrecio != null &&
		   maxPrecio != null)
		{
			return productoRepository.findProductsByFilter(nombre, marca, minPrecio, maxPrecio);			
		}
		
		if(!Util.validarCampoNulo(nombre))
		{
			return productoRepository.findByNombre(nombre);
		}
		if(!Util.validarCampoNulo(marca))
		{
			return productoRepository.findByMarca(marca);
		}
		if(minPrecio == null)
		{
			return productoRepository.findProductsByMinPrecio(minPrecio);
		}
		else
		{
			return productoRepository.findProductsByMaxPrecio(maxPrecio);
		}		
    }
	
	@PostMapping(path = "/AgregarProducto", produces = MediaType.APPLICATION_JSON_VALUE)
    public String agregarProducto(
    		@RequestParam (value = "cedula", required = false) Long cedula,
    		@RequestParam (value = "nombre", required = false) String nombre,
    		@RequestParam (value = "marca", required = false) String marca,
    		@RequestParam (value = "estado", required = false) String estado)
    		throws Exception
	{
		LOGGER.info("Inicio de Adicion de productos");
        List<Producto> productos=productoRepository.findProductsForAdd(nombre, marca, estado);
        if(productos != null && productos.size()>0)
        {
        	Producto producto= productos.get(0);
        	if(producto.getCantidadStock()>0)
        	{
        		CarritoCompra carritoCompra=new CarritoCompra();
        		carritoCompra.setCedulaCliente(cedula);
        		carritoCompra.setIdProducto(producto.getIdProducto());
        		carritoCompraRepository.save(carritoCompra);
        		return "Producto agregado exitosamente";
        	}
        	else
        	{
        		return "Producto no disponible en inventario";
        	}
        }
        else
        {
        	return "Producto no existente en inventario";
        }
    }
	
	@GetMapping(path = "/ConsultarProductosCarrito", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<?> consultarProductosCarrito(@RequestParam (value = "cedula", required = false) Long cedula)
    		throws Exception
	{
		LOGGER.info("Inicio de consulta de productos en carrito");

        return carritoCompraRepository.findByCedulaCliente(cedula);
    }
	
	@DeleteMapping(path = "/VaciarCarrito", produces = MediaType.APPLICATION_JSON_VALUE)
	@Transactional
    public String vaciarCarrito(@RequestParam (value = "cedula", required = false) Long cedula) 
    		throws Exception
	{
		LOGGER.info("Inicio de depuracion de carrito");

        if (carritoCompraRepository.deleteByCedulaCliente(cedula) == 0)
        {
        	return "Carrito de compra no existe para el cliente ingresado";
        }
        else
        {
        	return "Carrito de compra vacio";
        }        
    }
	
	@PostMapping(path = "/FinalizarCompra", produces = MediaType.APPLICATION_JSON_VALUE)
    public String finalizarCompra(
    		@RequestParam (value = "cedula", required = false) Long cedula) 
    		throws Exception
	{
		LOGGER.info("Finalizacion de compra");
        
        List<CarritoCompra> comprasCliente=carritoCompraRepository.findByCedulaCliente(cedula);
        
        Random r1 = new Random();
        Long idFactura=r1.nextLong();
        if(comprasCliente != null && comprasCliente.size()>0)
        {
        	comprasCliente.stream().forEach(compra->{
        		Producto producto=productoRepository.findByIdProducto(compra.getIdProducto());
        		if(producto != null)
        		{
        			Long cantidadStock=producto.getCantidadStock()-1;
        			producto.setCantidadStock(cantidadStock);
        			productoRepository.save(producto);
        			LOGGER.info("Stock actualizado de producto "+producto.getNombre());
        		}
        		compra.setIdFactura(idFactura);
        	});
        	carritoCompraRepository.saveAll(comprasCliente);
        	return String.format("Factura generada # [%s] para la cedula [%s]", idFactura, cedula);
        }
        else
        {
        	return String.format("Carrito de compra no existe para el cliente [%s]", cedula);
        }
    }
}
