package co.com.worldoffice.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.worldoffice.repository.ProductoRepository;

@RestController
@RequestMapping("/WorldOffice/BatchJob/V1.0")
public class BatchJobProductoController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job;
    
    @Autowired
    private ProductoRepository productoRepository;
    private static final Logger LOGGER = LogManager.getRootLogger();

	@GetMapping(path = "/WriteProducts", produces = MediaType.APPLICATION_JSON_VALUE)
    public String writeProduct() 
    		throws JobParametersInvalidException, JobExecutionAlreadyRunningException, 
    		JobRestartException, JobInstanceAlreadyCompleteException 
	{
        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(job, parameters);

        LOGGER.info("Estado de ejecucion de Job: " + jobExecution.getStatus());

        LOGGER.info("Job Batch en ejecucion...");
        while (jobExecution.isRunning()) {
        	LOGGER.info("...");
        }

        return "Job terminado - Estado "+jobExecution.getStatus();
    }
	
	@DeleteMapping(path = "/DeleteProducts", produces = MediaType.APPLICATION_JSON_VALUE)
    public String deleteProduct() 
    		throws Exception
	{
		LOGGER.info("Inidio de depuracion de productos");

        productoRepository.deleteAll();

        return "Depuracion de productos terminada";
    }
}
