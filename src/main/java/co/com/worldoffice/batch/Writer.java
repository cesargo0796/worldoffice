package co.com.worldoffice.batch;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.worldoffice.model.Producto;
import co.com.worldoffice.repository.ProductoRepository;

@Component
public class Writer implements ItemWriter<Producto> {

    private ProductoRepository productoRepository;
    private static final Logger LOGGER = LogManager.getRootLogger();

    @Autowired
    public Writer (ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public void write(List<? extends Producto> productos) throws Exception{
        LOGGER.info("Data Saved for Productos: {}", productos);
        productoRepository.saveAll(productos);
    }
}
