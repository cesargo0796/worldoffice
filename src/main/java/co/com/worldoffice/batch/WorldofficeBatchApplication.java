package co.com.worldoffice.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("co.com.worldoffice.batch.repository")
public class WorldofficeBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorldofficeBatchApplication.class, args);
	}

}
