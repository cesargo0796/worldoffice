package co.com.worldoffice.batch;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import co.com.worldoffice.model.Producto;

@Component
public class Processor implements ItemProcessor<Producto, Producto> {

    private static final Map<String, String> ESTADOS =
            new HashMap<>();
    private static final Logger LOGGER = LogManager.getRootLogger();

    public Processor() {
    	ESTADOS.put("Nuevo", "NUEVO");
        ESTADOS.put("Usado", "USADO");
    }

    @Override
    public Producto process(Producto producto) throws Exception {
        String estado = producto.getEstado();
        String estadoNuevo = ESTADOS.get(estado);
        producto.setEstado(estadoNuevo);
        producto.setFechaCreacion(new Date());
        LOGGER.info(String.format("Transformacion de [%s] a [%s]", estado, estadoNuevo));
        return producto;
    }
}
