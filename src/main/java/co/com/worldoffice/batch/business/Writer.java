package co.com.worldoffice.batch.business;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.worldoffice.batch.model.Producto;
import co.com.worldoffice.batch.repository.ProductoRepository;

@Component
public class Writer implements ItemWriter<Producto> {

    private ProductoRepository productoRepository;

    @Autowired
    public Writer (ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public void write(List<? extends Producto> productos) throws Exception{
        System.out.println("Data Saved for Productos: " + productos);
        productoRepository.saveAll(productos);
    }
}
