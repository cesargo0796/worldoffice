package co.com.worldoffice.batch.business;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import co.com.worldoffice.batch.model.Producto;

@Component
public class Processor implements ItemProcessor<Producto, Producto> {

    private static final Map<String, String> DEPT_NAMES =
            new HashMap<>();

    public Processor() {
        DEPT_NAMES.put("001", "Technology");
        DEPT_NAMES.put("002", "Operations");
        DEPT_NAMES.put("003", "Accounts");
    }

    @Override
    public Producto process(Producto producto) throws Exception {
//        String deptCode = producto.getDept();
//        String dept = DEPT_NAMES.get(deptCode);
//        producto.setDept(dept);
//        producto.setTime(new Date());
//        System.out.println(String.format("Converted from [%s] to [%s]", deptCode, dept));
        return producto;
    }
}
