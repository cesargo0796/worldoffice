package co.com.worldoffice.batch.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Producto 
{
    @Id
    private Integer idProducto;
    private String nombre;
    private String marca;
    private Double precio;
    private Long cantidadStock;
    private String estado;
    private Integer porcentajeDescuento;
      
	public Producto(Integer idProducto, String nombre, String marca, Double precio, Long cantidadStock, String estado,
			Integer porcentajeDescuento)
	{
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.marca = marca;
		this.precio = precio;
		this.cantidadStock = cantidadStock;
		this.estado = estado;
		this.porcentajeDescuento = porcentajeDescuento;
	}
	
	public Producto() {}
	
	
	public Integer getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public Long getCantidadStock() {
		return cantidadStock;
	}
	public void setCantidadStock(Long cantidadStock) {
		this.cantidadStock = cantidadStock;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getPorcentajeDescuento() {
		return porcentajeDescuento;
	}
	public void setPorcentajeDescuento(Integer porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}
}
