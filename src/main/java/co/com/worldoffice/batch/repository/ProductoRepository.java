package co.com.worldoffice.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.worldoffice.batch.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer> {
}
