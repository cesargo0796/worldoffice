package co.com.worldoffice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorldofficeBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorldofficeBatchApplication.class, args);
	}
}
