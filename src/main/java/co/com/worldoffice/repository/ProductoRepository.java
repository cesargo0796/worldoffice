package co.com.worldoffice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.com.worldoffice.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer> 
{
	public Producto findByIdProducto(Long idProducto);
	
	public List<Producto> findByNombre(String nombre);
	
	public List<Producto> findByMarca(String marca);
	
	@Query( value = "SELECT p FROM Producto p where p.precio BETWEEN  ?1 AND ?2")
	public List<Producto> findProductsByPrecio(Double minPrecio, Double maxPrecio);
	
	@Query( value = "SELECT p FROM Producto p where p.precio >= ?1")
	public List<Producto> findProductsByMinPrecio(Double minPrecio);
	
	@Query( value = "SELECT p FROM Producto p where p.precio <= ?1")
	public List<Producto> findProductsByMaxPrecio(Double maxPrecio);
	
	@Query( value = "SELECT p FROM Producto p where p.nombre = ?1 AND p.marca = ?2 AND p.precio BETWEEN  ?3 AND ?4")
	public List<Producto> findProductsByFilter(String nombre, String marca, Double minPrecio, Double maxPrecio);
	
	@Query( value = "SELECT p FROM Producto p where p.nombre = ?1 AND p.marca = ?2 AND p.estado = ?3")
	public List<Producto> findProductsForAdd(String nombre, String marca, String estado);
}
