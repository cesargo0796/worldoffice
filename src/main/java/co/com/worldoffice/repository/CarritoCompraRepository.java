package co.com.worldoffice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.worldoffice.model.CarritoCompra;

public interface CarritoCompraRepository extends JpaRepository<CarritoCompra, Integer> 
{
	public int deleteByCedulaCliente(Long cedulaCliente);	

	public List<CarritoCompra> findByCedulaCliente(Long cedulaCliente);
}
