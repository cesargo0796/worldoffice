package co.com.worldoffice.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Producto 
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idProducto;
    private String nombre;
    private String marca;
    private Double precio;
    private Long cantidadStock;
    private String estado;
    private Integer porcentajeDescuento;
    private Date fechaCreacion;
      
	public Producto(Long idProducto, String nombre, String marca, Double precio, Long cantidadStock, String estado,
			Integer porcentajeDescuento, Date fechaCreacion)
	{
		this.idProducto = idProducto;
		this.nombre = nombre;
		this.marca = marca;
		this.precio = precio;
		this.cantidadStock = cantidadStock;
		this.estado = estado;
		this.porcentajeDescuento = porcentajeDescuento;
		this.fechaCreacion = fechaCreacion;
	}
	
	public Producto(String nombre, String marca, Double precio) {
		super();
		this.nombre = nombre;
		this.marca = marca;
		this.precio = precio;
	}



	public Producto() {}
	
	
	public Long getIdProducto() {
		return idProducto;
	}
	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public Long getCantidadStock() {
		return cantidadStock;
	}
	public void setCantidadStock(Long cantidadStock) {
		this.cantidadStock = cantidadStock;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Integer getPorcentajeDescuento() {
		return porcentajeDescuento;
	}
	public void setPorcentajeDescuento(Integer porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
}
